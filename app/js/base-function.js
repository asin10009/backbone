function initTabs() {
    $('a', '.js-tab-control').click(function() {

        var parent = $(this).parent().parent(),
            contentAreaSelector = parent.attr('data-content-area');

        $('.js-tab-link-active', parent).removeClass('js-tab-link-active');
        $(this).addClass('js-tab-link-active');

        $('.js-tab-content' + contentAreaSelector).removeClass('js-tab-active');

        contentTab = $($(this).attr('data-content'));
        contentTab.addClass('js-tab-active');

        var colorName = $(this).attr('data-color');

        if (colorName !== undefined) {
            $(this).parents('.range-col-bottom-block').find('.color-name').html(colorName);
        }

        return false;
    })
}


function addBgToImgParent(imgSelector, parentClass) {
    var src, imgList = $(imgSelector, parentClass);

    for (var i = 0; i < imgList.length; i++) {
        src = $(imgList[i]).attr('src');
        $(imgList[i]).parent().css('background-image', 'url(\'' + src + '\')');
    }
}

function initAllAnchor(selector) {
    $(selector).click(function(event) {
        event.preventDefault();
        var url = $(this).attr('href');
        goToSelector(url, 750)
    })
}

function goToSelector(selector, time) {
    $('html, body').animate({
        scrollTop: $(selector).offset().top
    }, time);
}

function getFullScreenWidth() {
    var widthWithoutScrollBar = $("body").width(),
    fullScreenWidth = widthWithoutScrollBar + getScrollBarWidth();

    return fullScreenWidth;
}

function getScrollBarWidth() {
    var scrollbarWidth = function() {
        var a, b, c;
        if (c === undefined) {
            a = $('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');
            b = a.children();
            c = b.innerWidth() - b.height(99).innerWidth();
            a.remove();
        }
        return c;
    };
    return scrollbarWidth();
}