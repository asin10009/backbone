//creating model




var Book = Backbone.Model.extend({
    defaults: {
        name: '',
        year: '',
        popularity: '',
        price: 0
    },
    initialize: function() {
        console.log('book created');

        this.on('change:name', function() {
            console.log('name was changed, new name:' + this.get('name'));
        });

        this.on('invalid', function(model, error) {
            console.log(error);
        });
    },
    validate: function(attrs) {
        if (attrs.price < 0) {
            return 'Цена не может быть отрицательной'

        }
    }
});


var ad = new Book({
    name: 'Mazda',
    year: 2007,
    popularity: 2
});

ad.set('name', 'gadsdsadsda');
ad.set('price', -100, {
    validate: true
});

console.log(ad.get('price'));

var BookView = Backbone.View.extend({
    tagname: 'div',
    className: 'book',
    id: 'book-id'
});